from django.urls import path
from todos.views import (
    show_list,
    detail_list,
    create_list,
    update_list,
    delete_list,
    create_item,
    update_item,
)

urlpatterns = [
    path("", show_list, name="todo_list_list"),
    path("<int:id>/", detail_list, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/update/", update_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/update/", update_item, name="todo_item_update"),
]
